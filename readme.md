## R3C Wordpress UI

Plugins JS/CSS para incrementar a UI do Wordpress


Para usar a função de adicionar itens no menu Admin:

```
#!php
use R3C\Wordpress\Admin\Menu\Menu;
use R3C\Wordpress\Admin\Menu\Item;

$item = new Item();
$item->name = 'Testando';
$item->slug = 'testando';
$item->function = 'Testando\Controller\TestandoController@funcaoDeTeste';

Menu::addItem($item);
```
