<?php
namespace R3C\Wordpress\Admin\Menu;

class Menu {
    public static $items = [];

    public static function addItem($item)
    {
        self::$items[$item->slug] = $item;
    }

    public static function render()
    {
        foreach (self::$items as $item) {
            if (is_string($item->function) && strpos($item->function, '@') !== false) {
                $classAndFunction = explode('@', $item->function);
                $class = $classAndFunction[0];
                $function = $classAndFunction[1];

                $item->function = array(new $class, $function);
            }

            //add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
            add_menu_page($item->name, $item->name, $item->permission, $item->slug, $item->function, $item->icon, $item->position);

            if ( ! is_null($item->subitems) & count($item->subitems) > 0) {
                foreach ($item->subitems as $subitem) {
                    if (is_string($subitem->function) && strpos($subitem->function, '@') !== false) {
                        $classAndFunction = explode('@', $subitem->function);
                        $class = $classAndFunction[0];
                        $function = $classAndFunction[1];

                        $subitem->function = array(new $class, $function);
                    }

                    //add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function )
                    add_submenu_page($item->slug, $subitem->name, $subitem->name, $subitem->permission, $subitem->slug, $subitem->function);
                }
            }
        }
    }
}