/*#########################################
 * POPUP
 *########################################*/

Modules.popup = {
	start: function() {

	},

	open: function(url) {
		Modules.popup.lastUrl = url;

		var popupWindow = window.open(url);
		
		Modules.popup.check(popupWindow);
	},

	check: function(popupWindow){
        if (popupWindow) {
        	popupWindow.focus();

            if(/chrome/.test(navigator.userAgent.toLowerCase())){
                setTimeout(function () {
                    Modules.popup.isPopupBlocked(popupWindow);
                },200);
            } else {
                popupWindow.onload = function () {
                    Modules.popup.isPopupBlocked(popupWindow);
                };
            }
        } else {
            Modules.popup.displayError();
            Modules.popup.tries++;
        }
    },

    displayError: function() {
    	if (Modules.popup.tries < 1) {
        	Modules.noty.warning('Por favor, desative seu bloqueador de pop-ups. A janela será aberta automaticamente após essa ação.');
        }

        if (!Modules.popup.waiting) {
	        Modules.popup.waiting = true;

	        setTimeout(function() {
	        	Modules.popup.waiting = false;
	        	Modules.popup.open(Modules.popup.lastUrl);
	        }, 2000);
	    }
    },

    isPopupBlocked: function(popupWindow){
        if ((popupWindow.innerHeight > 0) == false) { 
        	Modules.popup.displayError();
        	Modules.popup.tries++;
        } else {
        	Modules.popup.tries = 0;
        	Modules.popup.waiting = false;
        }
    },

    lastUrl: '',

    tries: 0,

    waiting: false
}