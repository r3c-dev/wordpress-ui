/*#########################################
 * VALIDATOR
 *########################################*/

Modules.validator = {
	start: function() {

	},

	validate: function(form) {
		var valid = true;

		form.find('.required').each(function(index, element) {
			var selectize = $(element).find('.selectized');

			if (selectize.length > 0) {
				var control = $(element).find('.selectize-control');

				if (selectize.get(0).selectize.getValue() == '') {
					valid = false;
					control.addClass('error');
				} else {
					control.removeClass('error');
				}
			} else {
				var input = $(element).find('input');

				if (input.val() == '') {
					valid = false;
					$(element).addClass('error');
				} else {
					$(element).removeClass('error');
				}
			}
		});

		form.find('.mask').each(function(index, input) {
			var element = $(input).closest('.ui.field');

			if ($(input).length == 1) {
				if ($(input).val() != '') {
					if (!Modules.mask.validate($(input))) {
						valid = false;
						$(element).addClass('error');
					} else {
						$(element).removeClass('error');
					}
				}
			}
		});

		return valid;
	}

}