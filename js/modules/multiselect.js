/*#########################################
 * Multi select
 *########################################*/

Modules.multiselect = {
	start: function() {
		$('.multi-select').each(function(index, element) {
			$(element).multiSelect({ keepOrder: true });
		});
	},
}