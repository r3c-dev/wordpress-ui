/*#########################################
 * MASK
 *########################################*/

Modules.mask = {
	start: function() {
		this.cep($('.cep.mask'));
		this.cnpj($('.cnpj.mask'));
		this.cpf($('.cpf.mask'));
		this.data($('.data.mask'));
		this.mesAno($('.mes-ano.mask'));
		this.diaMes($('.dia-mes.mask'));
		this.horaMinuto($('.hora-minuto.mask'));
		this.numero3($('.numero-3.mask'));
		this.preco($('.preco.mask'));
		this.rg($('.rg.mask'));
		this.telefone($('.telefone.mask'));
	},

	cep: function(input) {
		input.mask('99999-999');
	},

	cnpj: function(input) {
		input.mask('99.999.999/9999-99');
	},

	cpf: function(input) {
		input.mask('999.999.999-99');
	},

	data: function(input) {
		input.mask('99/99/9999');
	},

	diaMes: function(input) {
		input.mask('99/99');
	},

	filter: function(input) {
		if (input.hasClass('mask cep')) {
			Modules.mask.cep(input);
		} else if (input.hasClass('mask cnpj')) {
			Modules.mask.cnpj(input);
		} else if (input.hasClass('mask cpf')) {
			Modules.mask.cpf(input);
		} else if (input.hasClass('mask data')) {
			Modules.mask.data(input);
		} else if (input.hasClass('mask mes-ano')) {
			Modules.mask.mesAno(input);
		} else if (input.hasClass('mask dia-mes')) {
			Modules.mask.diaMes(input);
		} else if (input.hasClass('mask hora-minuto')) {
			Modules.mask.horaMinuto(input);
		} else if (input.hasClass('mask numero-3')) {
			Modules.mask.numero3(input);
		} else if (input.hasClass('mask preco')) {
			Modules.mask.preco(input);
		} else if (input.hasClass('mask rg')) {
				Modules.mask.rg(input);
		} else if (input.hasClass('mask telefone')) {
			Modules.mask.telefone(input);
		}
	},

	horaMinuto: function(input) {
		input.mask('99:99');
	},

	mesAno: function(input) {
		input.mask('99/9999');
	},

	numero3: function(input) {
		input.mask('999');
	},

	mask: function(element) {
		if (element.tagName == 'input') {
			var input = element;
			Modules.mask.filter(input);
		} else {
			element.find('input').each(function(index, input) {
				var input = $(input);
				Modules.mask.filter(input);
			});
		}
	},

	preco: function(input) {
		input.priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
	},

	rg: function(input) {
		input.mask('99.999.999-9');
	},

	telefone: function(input) {
		input.mask('(99)9999-9999?9');
	},

	validate: function(input) {
		if (input.hasClass('mask data')) {
			var split = input.val().split('/');

			if (split.length < 3) {
				return false;
			}

			var m = moment(split[2] + '-' + split[1] + '-' + split[0]);

			return m.isValid();
		} else if (input.hasClass('mask mes-ano')) {
			var split = input.val().split('/');

			if (split.length < 2) {
				return false;
			}

			if (parseInt(split[0]) > 12) {
				return false;
			}

			return true;
		} else if (input.hasClass('mask dia-mes')) {
			var split = input.val().split('/');
			var mesesCom31 = [1, 3, 5, 7, 8, 10, 12];

			if (split.length < 2) {
				return false;
			}

			var dia = parseInt(split[0]);
			var mes = parseInt(split[1]);

			if (dia > 31) {
				return false;
			}

			if (mes > 31) {
				return false;
			}

			var tem31Dias = false;

			$.each(mesesCom31, function(index, value) {
				if (value == mes) {
					tem31Dias = true;
				}
			});

			if (!tem31Dias && dia == 31) {
				return false;
			}

			return true;
		} else if (input.hasClass('mask hora-minuto')) {
			var split = input.val().split(':');

			if (split.length < 2) {
				return false;
			}

			var hora = parseInt(split[0]);
			var minuto = parseInt(split[1]);

			if (hora > 23 || minuto > 59) {
				return false;
			}

			return true;
		}

		return true;
	}
}