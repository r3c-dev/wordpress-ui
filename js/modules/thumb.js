/*#########################################
 * THUMB
 *########################################*/

 Modules.thumb = {
 	start: function() {

 	},

 	generate: function(filename, thumb) {
 		var split = filename.split('.');
 		var extension = '.' + split[split.length - 1];

 		return filename.replace(extension, '_' + thumb + extension);
 	}
 }