/*#########################################
 * SELECTIZE
 *########################################*/

Modules.selectize = {
	start: function() {
		//Selectize puro
		$('.select.selectize').selectize({
			create: false
		});

		//Selectize com opção de criar um elemento
		$('.select.selectize-create').selectize({
			create: true,
			render: {
				option_create: function(data, escape) {
					return '<div class="create">Adicionar <strong>' + escape(data.input) + '</strong>&hellip;</div>';
				},
			}
		});

		//Selectize de Ordenação, com ícones
		$('.select.selectize-order').selectize({
			create: false,
			render: {
				item: function(item, escape) {
					var split = item.value.split(' ');
					var icon = 'sort ';

					if (split.length >= 2) {

						if (split.length >= 3) {
							if (split[split.length - 2] == 'alpha') {
								icon = icon + ' alphabet';
							} else if (split[split.length - 2] == 'numeric') {
								icon = icon + ' order';
							}
						} else {
							icon = icon + ' attributes';
						}

						if (split[split.length - 1] == 'asc') {
							icon = icon + ' ascending';
						} else if (split[split.length - 1] == 'desc') {
							icon = icon + ' descending';
						}

						return '<div class="option">' + escape(item.text) + ' <i class="' + icon + ' icon"></i></div>';
					} else {
						return '<div class="option">' + escape(item.text) + '</div>';
					}
				},
				option: function(item, escape) {
					var split = item.value.split(' ');
					var icon = 'sort ';

					if (split.length >= 2) {

						if (split.length >= 3) {
							if (split[split.length - 2] == 'alpha') {
								icon = icon + ' alphabet';
							} else if (split[split.length - 2] == 'numeric') {
								icon = icon + ' order';
							}
						} else {
							icon = icon + ' attributes';
						}

						if (split[split.length - 1] == 'asc') {
							icon = icon + ' ascending';
						} else if (split[split.length - 1] == 'desc') {
							icon = icon + ' descending';
						}

						return '<div class="option">' + escape(item.text) + ' <i class="' + icon + ' icon"></i></div>';
					} else {
						return '<div class="option">' + escape(item.text) + '</div>';
					}
				},
			}
		});

		//Selectize estilo tags
		this.tag($('.select.selectize-tag'));

		//Campo de busca do menu
 		$('.menu-intranet select.search-field').selectize({
			create: false,
			valueField: 'url',
		    labelField: 'nome',
		    searchField: 'nome',
		    optgroupField: 'tipo',
		    optgroups: [
		        {value: 'colaborador', label: 'Colaboradores'},
		        {value: 'condominio', label: 'Condomínios'},
		        {value: 'protocolo', label: 'Protocolos'},
		    ],
			render: {
				item: function(item, escape) {
					return '<div class="option">' + 
						'<span>' + escape(item.nome) + '</span>' +
						'</div>';
				},
				option: function(item, escape) {
					return '<div class="option">' + 
						'<span class="nome">' + escape(item.nome) + '</span>' +
						'</div>';
				},
				option_create: function(data, escape) {
					return '<div class="option">Buscar por <strong>' + escape(data.input) + '</strong>&hellip;</div>';
				},
				optgroup_header: function(data, escape) {
		            return '<div class="optgroup-header">' + escape(data.label) + '</div>';
		        }
			},
			load: function(query, callback) {
		        //if (!query.length) return callback();
		        $('.menu-intranet .busca .carregando').show();

		        $.ajax({
		            url: '/api/search',
		            type: 'GET',
		            dataType: 'json',
		            data: {
		                q: query
		            },
		            error: function() {
		                $('.menu-intranet .busca .carregando').hide();
		                callback();
		            },
		            success: function(item) {
		                $('.menu-intranet .busca .carregando').hide();
		                callback(item.data);
		            }
		        });
		    },
		    onChange: function(value) {
		    	if ($.trim(value) != '') {
		    		window.location = value;
		    	}
		    },
		    score: function(search) {
		    	return function() {
		    		return 1;
		    	}
		    }
		});
	},

	cidadeSelect: null,

	estado: function() {
		Modules.selectize.estadoSelect = $('.estado-field').selectize({
		    onChange: function(value) {
		        if (!value.length) return;

		        //var select_city = Modules.selectize.cidadeSelect[0].selectize;
		        var input = $(this).get(0).$input;
		        //console.log($(input).closest('form').find('.cidade-field'));
		        var select_city = $(input).closest('form').find('.cidade-field').get(0).selectize;

		        select_city.disable();
		        select_city.clearOptions();
		        select_city.load(function(callback) {
					Rest.cidade.search({estado: value}, function(data) {
						select_city.enable();
						callback(data.data);
					});
				});
		    }
		});

		Modules.selectize.cidadeSelect = $('.cidade-field').selectize({
		    valueField: 'nome',
		    labelField: 'nome',
		    searchField: ['nome']
		});

		if ($('.cidade-field').val() == '') {
			Modules.selectize.cidadeSelect[0].selectize.disable();
		} else {
			Modules.selectize.cidadeSelect[0].selectize.load(function(callback) {
				Rest.cidade.search({estado: $('.estado-field').val()}, function(data) {
					Modules.selectize.cidadeSelect[0].selectize.enable();
					callback(data.data);
				});
			});
		}
 	},

 	estadoSelect: null,

 	tag: function(input) {
 		input.selectize({
		    delimiter: ',',
		    persist: false,
		    create: function(input) {
		        return {
		            value: input,
		            text: input
		        }
		    },
		    render: {
				option_create: function(data, escape) {
					return '<div class="create">Adicionar <strong>' + escape(data.input) + '</strong>&hellip;</div>';
				},
			}
		});
 	}
}