/*#########################################
 * SEMANTIC UI
 *########################################*/

Modules.semantic = {
	start: function() {
		this.popup();
		this.checkbox();
	},

	checkbox: function() {
		$('.ui.checkbox')
		  .checkbox()
		;
	},

	popup: function() {
		$('.ui.popup.autostart')
		  .popup()
		;
	}
}