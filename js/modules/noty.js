/*#########################################
 * NOTY
 *########################################*/

Modules.noty = {
	start: function() {

	},

	error: function(text) {
		noty({
			maxVisible: 5,
			text: text,
			theme: 'flat',
			type: 'error',
		});
	},

	info: function(text) {
		noty({
			maxVisible: 5,
			text: text,
			theme: 'flat',
			type: 'information',
		});
	},

	success: function(text) {
		noty({
			maxVisible: 5,
			text: text,
			theme: 'flat',
			type: 'success',
			timeout: 5000,
		});
	},

	warning: function(text) {
		noty({
			maxVisible: 5,
			text: text,
			theme: 'flat',
			type: 'warning',
		});
	}
}