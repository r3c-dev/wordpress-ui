/*#########################################
 * DIMMER
 *########################################*/

Modules.dimmer = {
	start: function() {

	},

	error: function(text) {
		$('.module.ui.dimmer.error span').html(text);

		$('.module.ui.dimmer.error').addClass('active');

		setTimeout(function() {
			$('.module.ui.dimmer.error').removeClass('active');
		}, Modules.dimmer.options.fadeTime);
	},

	info: function(text) {
		$('.module.ui.dimmer.info span').html(text);

		$('.module.ui.dimmer.info').addClass('active');

		setTimeout(function() {
			$('.module.ui.dimmer.info').removeClass('active');
		}, Modules.dimmer.options.fadeTime);
	},

	options: {
		fadeTime: 2000
	},

	sending: function() {
		$('.module.ui.dimmer.sending').addClass('active');

		setTimeout(function() {
			$('.module.ui.dimmer.sending').removeClass('active');
		}, Modules.dimmer.options.fadeTime);
	},

	hideSending: function() {
		$('.module.ui.dimmer.sending').removeClass('active');
	},	

	showSending: function() {
		$('.module.ui.dimmer.sending').addClass('active');
	},

	success: function(text) {
		$('.module.ui.dimmer.success span').html(text);

		$('.module.ui.dimmer.success').addClass('active');

		setTimeout(function() {
			$('.module.ui.dimmer.success').removeClass('active');
		}, Modules.dimmer.options.fadeTime);
	},

	warning: function(text) {
		$('.module.ui.dimmer.warning span').html(text);

		$('.module.ui.dimmer.warning').addClass('active');

		setTimeout(function() {
			$('.module.ui.dimmer.warning').removeClass('active');
		}, Modules.dimmer.options.fadeTime);
	}
}