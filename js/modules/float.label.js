/*#########################################
 * FLOAT LABEL
 *########################################*/

Modules.floatLabel = {
	start: function() {
		document.addEventListener("DOMNodeInserted", function(event) {
			clearTimeout(Modules.floatLabel.autoLabelTimeout);

			Modules.floatLabel.autoLabelTimeout = setTimeout(function() {
				Modules.floatLabel.run();
			}, 100);
		});
	},

	autoLabelTimeout: null,

	mask: function(element, input) {
		if ($(input).val() != '') {
			$(element).attr('data-placeholder', $(input).attr('placeholder'));
		} else {
			$(element).attr('data-placeholder', '');
		}

		if (!$(input).attr('data-float-label')) {
			$(input).change(function() {
				Modules.floatLabel.mask(element, input);
			});
		}

		$(input).attr('data-float-label', true);
	},

	run: function() {
		$('.ui.field').each(function(index, element) {
			var input = $(element).find('> input[type="text"]');

			if (input.length > 0) {
				Modules.floatLabel.mask(element, input);
			} else {
				var input = $(element).find('select');

				if (input.length > 0) {
					Modules.floatLabel.mask(element, input);
				}
			}
		});
	},
}