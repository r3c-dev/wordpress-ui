<?php
use R3C\Wordpress\EventDispatcher\EventDispatcher;
use R3C\Wordpress\Admin\Menu\Menu;
use R3C\Wordpress\Twig\TwigFactory;

EventDispatcher::addListener(function() {
    add_action( 'admin_enqueue_scripts', 'r3cWordpressUIScripts' );
    add_action( 'admin_menu', 'r3cWordpressUIMenuRender' );
    add_action( 'admin_footer', 'r3cWordpressUIFooter');

    TwigFactory::addViewPath(__DIR__ . '/src/R3C/Wordpress/Admin/Views', 'wordpress-ui');
});

function r3cWordpressUIScripts() {
    $url = str_replace('/wp', '/', get_site_url());
    $js = $url . 'wp-content/plugins/wordpress-ui/js/';
    $css = $url . 'wp-content/plugins/wordpress-ui/';

	if ( current_user_can('manage_options')) {
        //DataTables
        wp_enqueue_script('r3c-wordpress-ui-datatables', $js . 'plugins/DataTables/media/js/jquery.dataTables.min.js', array(), '1.0');
        wp_enqueue_style('r3c-wordpress-ui-datatables', $js . 'plugins/DataTables/media/css/jquery.dataTables.min.css', array(), '1.0');

        //Dropzone
        wp_enqueue_script('r3c-wordpress-ui-dropzone', $js . 'plugins/dropzone/downloads/dropzone.min.js', array(), '1.0');
        wp_enqueue_style('r3c-wordpress-ui-dropzone', $js . 'plugins/dropzone/downloads/css/dropzone.css', array(), '1.0');

        //Fancybox
        wp_enqueue_script('r3c-wordpress-ui-fancybox', $js . 'plugins/fancybox/source/jquery.fancybox.pack.js', array(), '1.0');
        wp_enqueue_style('r3c-wordpress-ui-fancybox', $js . 'plugins/fancybox/source/jquery.fancybox.css', array(), '1.0');

        //Handlebars
        wp_enqueue_script('r3c-wordpress-ui-handlerbars', $js . 'plugins/handlebars/handlebars.min.js', array(), '1.0');

        //Masked Inputs
        wp_enqueue_script('r3c-wordpress-ui-masked-inputs', $js . 'plugins/jquery-maskedinputs/dist/jquery.maskedinput.min.js', array(), '1.0');

        //Price Format
        wp_enqueue_script('r3c-wordpress-ui-priceformat', $js . 'plugins/jquery-price-format/jquery.price_format.min.js', array(), '1.0');

        //Serialize Object
        wp_enqueue_script('r3c-wordpress-ui-serialize-object', $js . 'plugins/jquery-serialize-object/dist/jquery.serialize-object.min.js', array(), '1.0');

        //Rest
        wp_enqueue_script('r3c-wordpress-ui-rest', $js . 'plugins/jquery.rest/dist/1/jquery.rest.min.js', array(), '1.0');

        //Moment
        wp_enqueue_script('r3c-wordpress-ui-moment', $js . 'plugins/moment/min/moment-with-locales.min.js', array(), '1.0');

        //Noty
        wp_enqueue_script('r3c-wordpress-ui-noty', $js . 'plugins/noty/js/noty/packaged/jquery.noty.packaged.min.js', array(), '1.0');

        //Selectize
        wp_enqueue_script('r3c-wordpress-ui-selectize', $js . 'plugins/selectize/dist/js/standalone/selectize.min.js', array(), '1.0');
        wp_enqueue_style('r3c-wordpress-ui-selectize', $js . 'plugins/selectize/dist/css/selectize.default.css', array(), '1.0');

        //Manual
        wp_enqueue_script('r3c-wordpress-ui-bootstrap', $js . 'plugins/manual/bootstrap.min.js', array(), '1.0');
        wp_enqueue_script('r3c-wordpress-ui-jquery-ui', $js . 'plugins/manual/jquery-ui-1.10.4.custom.min.js', array(), '1.0');
        wp_enqueue_script('r3c-wordpress-ui-multi-select', $js . 'plugins/manual/jquery.multi-select.js', array(), '1.0');

        //R3C Wordpress UI
        wp_enqueue_script('r3c-wordpress-ui', $js . 'code.min.js', array(), '1.0');
        wp_enqueue_style('r3c-wordpress-ui', $css . 'style.css', array(), '1.0');
	}
}

function r3cWordpressUIMenuRender()
{
    Menu::render();
}

function r3cWordpressUIFooter()
{
    echo TwigFactory::render('@wordpress-ui/footer.twig');
}

?>